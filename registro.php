

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/register.css">
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header ">
                <a class="navbar-brand" rel="home" href="#" title="Buy Sell Rent Everyting">
                    <img style="max-width:200px; margin-top: -30px;" src="img/logo.jpg">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <form class="navbar-form navbar-right">
                    <button type="submit" class="btn btn-default">Login</button>
                </form>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="container">

        <h1>User Registration</h1>
        <form action="logica/guardar.php" method="POST" class="form-group row" role="form">
            <div class="col-xs-6">
                <hr>
            </div>
            <br><br><br>
            <div class="col-xs-3">
                <input type="text" class="form-control" id="" name="FirtsName" placeholder="Name">
            </div>
            <div class="col-xs-3">
                <input type="text" class="form-control" id="" name="LastName" placeholder="Last Name">
            </div>
            <br><br><br>
            <div class="col-xs-3">
                <input type="text" class="form-control" id="" name="Email" placeholder="Email">
            </div>
            <div class="col-xs-3">
                <input type="password" class="form-control" id="" name="Password" placeholder="Password">
            </div>
            <br><br><br>
            <div class="col-xs-6">
                <label class="sr-only" for="">Address</label>
                <input type="text" class="form-control" id="" name="Address" placeholder="Address">
            </div>
            <br><br><br>
            <div class="col-xs-6">
                <label class="sr-only" for="">Address2</label>
                <input type="text" class="form-control" id="" name="Address2" placeholder="Address 2">
            </div>
            <br><br><br>
            <div class="col-xs-3">
                <label class="sr-only" for="">pais</label>
                <select name="pais" class="form-control">
                    <option value="Costa Rica" selected>Costa Rica</option>
                    <option value="Estados Unidos">Estados Unidos</option>
                    <option value="Alemania">Alemania</option>
                </select>
            </div>
            <div class="col-xs-3">
                <label class="sr-only" for="">city</label>
                <input type="text" class="form-control" id="" name="city" placeholder="City">
            </div>
            <br><br><br>
            <div class="col-xs-3">
                <label class="sr-only" for="">Email</label>
                <input type="text" class="form-control" id="" name="PostalCode" placeholder="Zip/Postal Code">
            </div>
            <div class="col-xs-3">
                <label class="sr-only" for="">Password</label>
                <input type="text" class="form-control" id="" name="Phone" placeholder="Phone Number">
            </div>
            <br><br><br>
            <div class="col-xs-6">
                <hr>
            </div>
            <br><br><br>
            <button type="submit" class="btn btn-primary">Sign up</button>
        </form>
    </div>

    <footer>
        <!-- <hr id="hr3">
        <div class="contenedor-footer">
            <div class="content-foo">
                <h4>Incio</h4>
            </div>
            <div class="content-fo">
                <h4>|</h4>
            </div>
            <div class="content-foo">
                <h4>Cambalache</h4>
            </div>
            <div class="content-fo">
                <h4>|</h4>
            </div>
            <div class="content-foo">
                <h4>Ingresar</h4>
            </div>
        </div>
        <h2 class="titulo-final">&copy; Cambalache.net</h2>-->

        <nav >
            <hr>
            <div class="container text-center">
                <div class="content-fooa">
                    <h4> <a href="">My Cover</a> | <a href="">About</a> | <a href="">Help</a></h4>
                </div>
                <p class="navbar-text col-md-12 col-sm-12 col-xs-12">&copy; My News Cover</p>
            </div>
        </nav>
    </footer>
</body>

</html>