<?php
  // session_start();
  // if ($_SESSION && $_SESSION['user']){
  //   //user already logged in
  //   header('Location: /dashboard.php');
  // }

  $message = "";
  if(!empty($_REQUEST['status'])) {
    switch($_REQUEST['status']) {
      case 'login':
        $message = 'User does not exists';
      break;
      case 'error':
        $message = 'There was a problem inserting the user';
      break;
    }
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/login.css">

    <title>Inicio de sesion</title>
</head>

<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header ">
                <a class="navbar-brand" rel="home" href="#" title="Buy Sell Rent Everyting">
                    <img style="max-width:300px; margin-top: -50px;" src="img/logo.jpg">
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <form class="navbar-form navbar-right">
                    <button type="submit" class="btn btn-default">Login</button>
                </form>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <div class="container">
        <div class="msg">
            <?php echo $message; ?>
        </div>
        <h1>User Login</h1>
        <hr id="hr3">
        <form action="logica.php" method="POST" class="form-inline" role="form">
            <div class="form-group">
                <label class="sr-only" for="">Username</label>
                <input type="text" class="form-control" id="" name="username" placeholder="Your username">
            </div>
            <br><br><br>
            <div class="form-group">
                <label class="sr-only" for="">Password</label>
                <input type="password" class="form-control" id="" name="password" placeholder="Your password">
            </div>
            <hr id="hr3">
            <button type="submit" class="btn btn-primary">Login</button>
            <p>If you don't have an account,
                <a href="registro.php" title="registro">Sign Up Here</a>
            </p>
        </form>
    </div>
    <footer>
           <!-- <hr id="hr3">
            <div class="contenedor-footer">
                <div class="content-foo">
                    <h4>My Cover</h4>
                </div>
                <div class="content-fo">
                    <h4>|</h4>
                </div>
                <div class="content-foo">
                    <h4>About</h4>
                </div>
                <div class="content-fo">
                    <h4>|</h4>
                </div>
                <div class="content-foo">
                    <h4>Help</h4>
                </div>
            </div>
            <h2 class="titulo-final"> <strong> &copy;</strong> My News Cover</h2>-->
    <nav class="navbar navbar-default  navbar-fixed-bottom" role="navigation">
        <div class="container text-center">
            <div class="content-fooa">
                <h4> <a href="">My Cover</a> | <a href="">About</a> | <a href="">Help</a></h4>
            </div>
            <p class="navbar-text col-md-12 col-sm-12 col-xs-12">&copy; My News Cover by Geberth Daniel Alfaro Sequeira</p>
        </div>
    </nav>
    </footer>
</body>

</html>